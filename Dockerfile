FROM php:7.3-apache
LABEL maintainer="Sander Van Damme"

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Brussels

RUN apt update && apt install -y --no-install-recommends \
    git \
    libicu-dev \
    libtidy-dev \
    libzip-dev \
    zlib1g-dev \
    && apt clean

RUN cd /var/www/html \
    && git clone https://bitbucket.org/fivefilters/full-text-rss.git ./ \
    && rm -rf .git \
    && sed -i 's/\$options->max_entries = 10;/\$options->max_entries = 9999;/g' ./config.php \
    && cd ./site_config/standard/ \
    && git init \
    && git remote add origin https://github.com/fivefilters/ftr-site-config.git \
    && git fetch origin master \
    && git reset --hard origin/master \
    && sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf \
    && docker-php-ext-install intl \
    && docker-php-ext-install tidy \
    && docker-php-ext-install zip \
    && docker-php-ext-enable intl tidy zip

COPY ./update.sh /update.sh
EXPOSE 8080