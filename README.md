# Full-Text RSS
Full-Text RSS by [FiveFilters](https://www.fivefilters.org) in an OCI container.

## Supported Architectures
`amd64`

## Parameters
| Parameter | Function |
| :----: | --- |
| `-p 8080` | The port for the Full-Text RSS web interface. |

## Setup

### docker cli
```
docker run -d \
  --name full-text-rss \
  -p 8080:8080 \
  registry.gitlab.com/sander-vd/full-text-rss
```

### docker-compose
```
version: "2"
services:
  full-text-rss:
    image: registry.gitlab.com/sander-vd/full-text-rss
    container_name: full-text-rss
    ports:
      - 8080:8080
    restart: unless-stopped
```

## Usage
Access the webui at `<your-ip>:8080`.  
Refer to the [documentation of Five Filters](https://help.fivefilters.org/full-text-rss/) for more information.

### Updating the site-specific extraction rules
To update the site-specific extraction rules, run `/update.sh` in the container.
```
docker exec full-text-rss /update.sh
```

## Built with
- [Full-Text RSS](https://bitbucket.org/fivefilters/full-text-rss) (AGPLv3)
- [Site-specific extraction rules](https://github.com/fivefilters/ftr-site-config) (public domain)
